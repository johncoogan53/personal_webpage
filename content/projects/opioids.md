+++
title = "RUST SQLite CLI"
date = 2021-11-12
+++

This project found at this [[project link](https://github.com/johncoogan53/Rust-SQLite-CLI)] uses video game sales data taken from kaggle in a similar fashion as the python project. It contains a lib.rs file which creates a rusqlite table, imports the csv data to the database, and queries that database. The main.rs file simply calls each of those lib functions while running a profiler for performance metrics. By implementing a query function as a command line tool, this project allows high performance CRUD operations to be dynamically conducted locally through rust and SQLite.