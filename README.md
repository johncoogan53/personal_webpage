# Personal Website generated with Zola hosted on S3
Demo Video: https://www.youtube.com/watch?v=1kX62b3KQ0w

This personal website is generated with zola and hosted on Amazon S3 with Route 53. Zola is a unique and user friendly static site generator which is written in Rust and allows for the use of themes to easily modify existing websites to fit the needs of the user. This website is currently using the Blow theme and has been modified to include a landing page, about section, and projects section.
![Alt text](Projects_demo.gif) 

This website also contains external links to professional accounts for anyone to view.
![Alt text](Links_demo.gif)

The website is currently hosted on S3 and leverages the platforms CI/CD framework to automatically push updates to the website when changes are made to the repository. The website can be found at:

www.johncoogan53.com

